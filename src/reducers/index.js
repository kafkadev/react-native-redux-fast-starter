import { combineReducers } from 'redux';
import { navReducer } from "./Navigation"
import { reducer as formReducer } from 'redux-form'
const AppReducer = combineReducers({
  nav: navReducer,
  form: formReducer
});

export default AppReducer;
