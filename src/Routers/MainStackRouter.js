import React, { Component } from "react";
import {StartC} from '../helpers/AllConnect'
import Second from "../components/Second";
import { StackNavigator } from "react-navigation";
import { Platform} from 'react-native';
export default (StackNav = StackNavigator({
  Start: { screen: StartC },
  Second: { screen: Second }
},{
initialRouteName: "Start",
headerMode: "none",
mode: Platform.OS === 'ios' ? 'modal' : 'card',
}));
