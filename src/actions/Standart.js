import { DISCOUNTS_TYPES as types } from "./types";
import { ConfigData } from "../helpers/Config";

export const getData = () => {
  return fetch(ConfigData.api_url + "/demolist")
    .then(response => response.json())
    .then(responseJson => {
      return fetchData(responseJson);
    })
    .catch(error => {
      console.error(error);
    });
};

function fetchData(payloads) {
  return {
    type: types.FETCH_DATA,
    payloads: payloads
  };
}
export const addToGroceryList = (item) => {
    return { type: 'ADD_GROCERY', item };
}

export const removeFromGroceryList = (id) => {
    return { type: 'REMOVE_GROCERY', id };
}

export const addToTodoList = (item) => {
    return { type: 'ADD_TODO', item };
}

export const removeFromTodoList = (id) => {
    return { type: 'REMOVE_TODO', id };
}